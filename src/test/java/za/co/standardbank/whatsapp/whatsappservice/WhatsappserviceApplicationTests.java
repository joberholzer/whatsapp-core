package za.co.standardbank.whatsapp.whatsappservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import za.co.standardbank.whatsapp.Controllers.WhatsappController;
import za.co.standardbank.whatsapp.Models.Event;
import za.co.standardbank.whatsapp.Models.Messages;
import za.co.standardbank.whatsapp.Models.MoText;
import za.co.standardbank.whatsapp.Models.RecieveWhatsapp;
import za.co.standardbank.whatsapp.Models.SendWhatsapp;
import za.co.standardbank.whatsapp.Models.Sms;
import za.co.standardbank.whatsapp.Services.WhatsAppService;

//@RunWith(SpringRunner.class)
@SpringBootTest
class WhatsappserviceApplicationTests 
{
	@Test
	public void HealthCheck()
	{   
		/* This is a test to see that the controller is available.*/
		RabbitTemplate rabbitTemplate = new RabbitTemplate();
		WhatsappController _WhatsappController = new WhatsappController(rabbitTemplate);

		try
		{
			ResponseEntity<?> response = _WhatsappController.HealthCheck();
			
			assertEquals(response.getStatusCode().value(), 200);
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}

	@Test
	public void SendMessageToWhatsapp()
	{   
		/* This test is to send a message to whatsapp from the whatsapp service this will 
		 not go to the client as the client needs to initiate the chat first, this test however
		 shows that if the message has been send to whatsapp it would have accepted the message
		 and the message would go out to the client. */

		WhatsAppService _whatsAppService = new WhatsAppService();
		List<Messages> messages = new ArrayList<Messages>();

		Messages message = new Messages();
		message.setChannel("whatsapp");
		message.setContent("This is the Unit Test.");
		message.setTo("27820889391");

		messages.add(message);

		SendWhatsapp whatsapp = new SendWhatsapp();
		whatsapp.setIntegrationId("ff808081703e50060170a595203c78cd");
		whatsapp.setIntegrationName("warm");
		whatsapp.setMessages(messages);
		try
		{
			HttpResponse response = _whatsAppService.SendMessage(whatsapp);

			assertEquals(response.getStatusLine().getStatusCode(), 202);
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	@Test
	public void SendMessageFromWhatsappToQueue()
	{   
		/* This is to ensure that when a message is recieved from 
		whatsapp it will be send to the queue so that the bussiness core service can 
		set it into the DB.*/

		RabbitTemplate rabbitTemplate = new RabbitTemplate();
		WhatsappController _WhatsappController = new WhatsappController(rabbitTemplate);
		List<MoText> messages = new ArrayList<MoText>();
		Event event = new Event();
		
		MoText message = new MoText();
		message.setMessageId("1c897104a13f44888ade7b6c1a3b2bc8");
		message.setRelatedMessageId(null);
		message.setRelatedClientMessageId(null);
		message.setFrom("27820889391");
		message.setTo("27219107820"); // This will need to change to the Prod number.
		message.setTimestamp(LocalDateTime.now().toString());
		message.setEncryptionKey(null);
		message.setCharset(null);
		message.Sms = new Sms();
		message.setChannel("whatsapp");
		message.setContent("This is the Unit Test.");
		message.setTo("27820889391");

		messages.add(message);
		event.setMoText(messages);

		RecieveWhatsapp whatsapp = new RecieveWhatsapp();
		whatsapp.setIntegrationId("ff808081703e50060170a595203c78cd");
		whatsapp.setIntegrationName("warm");
		whatsapp.setEvent(event);
		try
		{
			ResponseEntity<?> response = _WhatsappController.ReceiveFromWhatsappNew(whatsapp);
			
			assertEquals(response.getStatusCode(), 200);
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}
}
