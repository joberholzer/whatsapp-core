package za.co.standardbank.whatsapp.Helpers;

import com.google.gson.Gson;

public class JsonConverter {

    public String JsonConvert(Object j){
        Gson gson = new Gson();
        String json = gson.toJson(j);
      
        return json;
      }
}