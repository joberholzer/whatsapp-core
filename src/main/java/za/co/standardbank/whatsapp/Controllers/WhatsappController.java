package za.co.standardbank.whatsapp.Controllers;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import za.co.standardbank.whatsapp.Helpers.ConstantFile.ApplicationConstant;
import za.co.standardbank.whatsapp.Models.RecieveWhatsapp;
import za.co.standardbank.whatsapp.RabbitMq.MessageSender;
import za.co.standardbank.whatsapp.config.ApplicationConfigReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
@CrossOrigin
public class WhatsappController
{

  private static final Logger log = LoggerFactory.getLogger(WhatsappController.class);
  
  @Autowired
  private final RabbitTemplate rabbitTemplate;

  @Autowired
  private ApplicationConfigReader applicationConfig;

  @Autowired
	private za.co.standardbank.whatsapp.RabbitMq.MessageSender messageSender;

  public ApplicationConfigReader getApplicationConfig() 
  {
		return applicationConfig;
	}

  @Autowired
  public void setApplicationConfig(ApplicationConfigReader applicationConfig) 
  {
		this.applicationConfig = applicationConfig;
  }
    
  @Autowired
  public WhatsappController(final RabbitTemplate rabbitTemplate) 
  {
		this.rabbitTemplate = rabbitTemplate;
  }
    
  public MessageSender getMessageSender() 
  {
		return messageSender;
  }
    
  @Autowired
  public void setMessageSender(MessageSender messageSender) 
  {
    this.messageSender = messageSender;
  }
  
  @GetMapping("api/receive/HealthCheck")
  public ResponseEntity<String> HealthCheck()
  {
      String online ="Online";

      return new ResponseEntity<String>(online,HttpStatus.OK);
  }

  @PostMapping("api/receive/message")
  public ResponseEntity<?> ReceiveFromWhatsappNew(@RequestBody RecieveWhatsapp receiveMessageCallback)
  {
    String exchange = getApplicationConfig().getFromWhatsappExchange();
    String routingKey = getApplicationConfig().getFromWhatsappRoutingKey();

    /* Sending to Message Queue */
    try {
          messageSender.sendMessage(rabbitTemplate, exchange, routingKey, receiveMessageCallback);

          return new ResponseEntity<String>(ApplicationConstant.IN_QUEUE, HttpStatus.OK);   
        } 
        catch (Exception ex) 
        {
          log.error("Exception occurred while sending message to the queue. Exception= {}", ex);

          return new ResponseEntity(ApplicationConstant.MESSAGE_QUEUE_SEND_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
        }
  } 
}