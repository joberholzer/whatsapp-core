package za.co.standardbank.whatsapp.Interfaces;

import org.apache.http.HttpResponse;

import za.co.standardbank.whatsapp.Models.*;

public interface WhatsAppServiceInterface
{
    HttpResponse SendMessage(SendWhatsapp whatsapp)throws Exception;
}