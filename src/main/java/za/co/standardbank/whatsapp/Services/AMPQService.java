package za.co.standardbank.whatsapp.Services;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.AMQP.BasicProperties;

import za.co.standardbank.whatsapp.Helpers.JsonConverter;
import za.co.standardbank.whatsapp.Interfaces.AMPQServiceInterface;
import za.co.standardbank.whatsapp.Models.RecieveWhatsapp;
import za.co.standardbank.whatsapp.Models.SendWhatsapp;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;

public class AMPQService implements AMPQServiceInterface
{
  private JsonConverter convert = new JsonConverter();
  private WhatsAppService _WhatsAppService = new WhatsAppService();
  private String Msg;
  
  private ConnectionFactory CreateQueueConnection()
  {
      ConnectionFactory factorys = new ConnectionFactory();
      factorys.setUsername("kvigcuph");
      factorys.setPassword("cmpzqFkmxtXw-7z2G5R6eqtEXnfRrRdi");
      factorys.setVirtualHost("kvigcuph");
      factorys.setHost("crow-01.rmq.cloudamqp.com");
      factorys.setPort(5672);
      factorys.setAutomaticRecoveryEnabled(true);
  
      return factorys;
  }

  public RecieveWhatsapp SetMessageToQue(RecieveWhatsapp callBack)
  {   
    BasicProperties x = new BasicProperties();
    String json = convert.JsonConvert(callBack);

    try 
    {
      ConnectionFactory factory = CreateQueueConnection();
      Connection conn = factory.newConnection();
      Channel channel = conn.createChannel();
  
      channel.exchangeDeclare("FromWhatsapp", "direct", true);
      String queueName = channel.queueDeclare().getQueue();
      System.out.println(queueName);
      channel.queueBind(queueName, "FromWhatsapp", "FromWhatsapp");
      System.out.println("Producing message: " + json + " in thread: " + Thread.currentThread().getName()+"Bytes:"+ json.toString().getBytes());
      byte[] bytes = json.getBytes();
      String str = new String(bytes, "UTF-8");
      System.out.println(str);
      channel.basicPublish("FromWhatsapp", "FromWhatsapp", x, json.getBytes());
      System.out.println(channel.basicGet("FromWhatsapp", true));
      System.out.println(channel.basicConsume("FromWhatsapp",new DefaultConsumer(channel)));
      channel.close();
      conn.close();

      RecieveFromQueue();
    }
    catch (IOException | TimeoutException e) 
    {
      e.printStackTrace();
    }         
      return callBack;
  }

  private void RecieveFromQueue()
  {
    System.out.println("Recieve From Queue.");

    try 
    {
      ConnectionFactory factory = CreateQueueConnection();
      Connection conn = factory.newConnection();
      Channel channel = conn.createChannel();
      channel.queueDeclare("ToWhatsapp", true, false, false, null);
      System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

      DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        Msg = new String(delivery.getBody(), "UTF-8");
        System.out.println(" [x] Received From Queue -> " + Msg );
        Gson gson = new Gson();
        System.out.println("Message Before send -> " + Msg );
        SendWhatsapp whatsapp1 = new ObjectMapper().readValue(Msg, SendWhatsapp.class);
        SendWhatsapp whatsapp = gson.fromJson(Msg, SendWhatsapp.class);
        if(whatsapp != null){
          try
          {
            _WhatsAppService.SendMessage(whatsapp1);
          }
          catch (Exception e)
          {
            System.err.println("Error -> "+ e );
          }
        }
    };

    //Send whatsapp message from queue
    channel.basicConsume("ToWhatsapp", true, deliverCallback, consumerTag -> { });
    }
    catch (Exception e)
    {
      System.err.println(e);
    }
  }
}