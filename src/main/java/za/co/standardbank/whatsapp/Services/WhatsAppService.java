package za.co.standardbank.whatsapp.Services;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;

import za.co.standardbank.whatsapp.Helpers.ConstantFile;
import za.co.standardbank.whatsapp.Helpers.JsonConverter;
import za.co.standardbank.whatsapp.Interfaces.WhatsAppServiceInterface;
import za.co.standardbank.whatsapp.Models.SendWhatsapp;
import za.co.standardbank.whatsapp.config.ApplicationConfigReader;

public class WhatsAppService implements WhatsAppServiceInterface
{
    static ConstantFile _constantFile = new ConstantFile();
    private JsonConverter convert = new JsonConverter();

    @Autowired
	private ApplicationConfigReader applicationConfig;

    public HttpResponse SendMessage(SendWhatsapp whatsapp)throws Exception
    {
        System.out.println("In Send Message.");
        HttpClient httpClient = new DefaultHttpClient();

        String json = convert.JsonConvert(whatsapp);
        System.out.println("Send:" + json);

        try
        {
            //Define a postRequest request
            HttpPost postRequest = new HttpPost("https://platform.clickatell.com/v1/message"); 

            //Set the API media type in http content-type header
            postRequest.addHeader("content-type", "application/json");
            postRequest.addHeader("Authorization", "OOw8zjH-RBmN8g31AuLsAA=="); // Need to add API Key  
            postRequest.addHeader("Accept", "application/json");

            //Set the request post body           
            StringEntity Entity = new StringEntity(json);
            postRequest.setEntity(Entity);
            
                
            //Send the request; It will immediately return the response in HttpResponse object if any
            HttpResponse response = httpClient.execute(postRequest);
                
            //verify the valid error code first
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 202) 
            {
                System.out.println("Failed with HTTP error code : " + statusCode);
                throw new RuntimeException("Failed with HTTP error code : " + statusCode);               
            }
            System.out.println("Response Code" + response.getStatusLine().getStatusCode());
            System.out.println("Response" + response);

            return response;           
        }
        finally
        {
            //Important: Close the connect
            httpClient.getConnectionManager().shutdown();
        }
    } 
}