package za.co.standardbank.whatsapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration
@PropertySource("classpath:application.properties")
@Component
public class ApplicationConfigReader {
	
	@Value("${fromWhatsapp.exchange.name}")
	private String fromWhatsappExchange;
	
	@Value("${fromWhatsapp.queue.name}")
	private String fromWhatsappQueue;
	
	@Value("${fromWhatsapp.routing.key}")
	private String fromWhatsappRoutingKey;

	@Value("${toWhatsapp.exchange.name}")
	private String toWhatsappExchange;
	
	@Value("${toWhatsapp.queue.name}")
	private String toWhatsappQueue;
	
	@Value("${toWhatsapp.routing.key}")
    private String toWhatsappRoutingKey;
    
    @Value("${clickatell.routing.url}")
	private String clickatellRoutingUrl;
	
	@Value("${clickatell.routing.apikey}")
    private String clickatellRoutingAPIKey;
    
    public String getFromWhatsappExchange() {
		return fromWhatsappExchange;
	}

	public void setFromWhatsappExchange(String FromWhatsappExchange) {
		this.fromWhatsappExchange = FromWhatsappExchange;
	}

	public String getFromWhatsappQueue() {
		return fromWhatsappQueue;
	}

	public void setFromWhatsappQueue(String FromWhatsappQueue) {
		this.fromWhatsappQueue = FromWhatsappQueue;
	}

	public String getFromWhatsappRoutingKey() {
		return fromWhatsappRoutingKey;
	}

	public void setFromWhatsappRoutingKey(String FromWhatsappRoutingKey) {
		this.fromWhatsappRoutingKey = FromWhatsappRoutingKey;
	}

	public String getToWhatsappExchange() {
		return toWhatsappExchange;
	}

	public void setToWhatsappExchange(String ToWhatsappExchange) {
		this.toWhatsappExchange = ToWhatsappExchange;
	}

	public String getToWhatsappQueue() {
		return toWhatsappExchange;
	}

	public void setToWhatsappQueue(String ToWhatsappQueue) {
		this.toWhatsappExchange = ToWhatsappQueue;
	}

	public String getToWhatsappRoutingKey() {
		return toWhatsappExchange;
	}

	public void setToWhatsappRoutingKey(String ToWhatsappRoutingKey) {
		this.toWhatsappExchange = ToWhatsappRoutingKey;
	}

	public void setClickatellRoutingUrl(String clickatellRoutingUrl) {
		this.clickatellRoutingUrl = clickatellRoutingUrl;
	}

	public String getClickatellRoutingUrl() {
		return clickatellRoutingUrl;
	}

	public void setClickatellRoutingAPIKey(String clickatellRoutingAPIKey) {
		this.clickatellRoutingAPIKey = clickatellRoutingAPIKey;
	}

	public String getClickatellRoutingAPIKey() {
		return clickatellRoutingAPIKey;
	}

}
