package za.co.standardbank.whatsapp.Models;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Event implements Serializable
{
    private static final long serialVersionUID = 1L;
    public List<MoText> moText;
    public List<StatusCallBack> messageStatusUpdate;
}