package za.co.standardbank.whatsapp.Models;

import java.io.Serializable;

import lombok.*;

@Setter
@Getter
public class StatusCallBack implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String integrationId;
    private String integrationName;
    private String timestamp;
	private String statusCode;
    private String status;
    private String messageId;
    private String clientMessageId;
}