package za.co.standardbank.whatsapp.Models;

import java.io.Serializable;
import java.util.List;

import lombok.*;

@Setter
@Getter
public class SendWhatsapp implements Serializable
{  
    private static final long serialVersionUID = 1L;
    private String integrationName;
    private String integrationId;   
    private List<Messages> messages;
    private String encryptionKey; 
    private String sha256Hash;
}