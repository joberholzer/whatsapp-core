package za.co.standardbank.whatsapp.Models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hsm implements Serializable{

	private static final long serialVersionUID = 1L;
	private String template;
    private String[] parameters;
}