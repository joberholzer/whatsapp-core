package za.co.standardbank.whatsapp.Models;

import java.io.Serializable;

import lombok.*;

@Setter
@Getter
public class Messages implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String channel;
    private String to;
    private String content;
    private String contentType;
    private String caption;
    private String clientMessageId;
    private String previewFirstUrl;
    private Hsm hsm;
}