package za.co.standardbank.whatsapp.RabbitMq;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import za.co.standardbank.whatsapp.Helpers.ConstantFile.ApplicationConstant;
import za.co.standardbank.whatsapp.Models.Messages;
import za.co.standardbank.whatsapp.Models.SendWhatsapp;
import za.co.standardbank.whatsapp.Services.WhatsAppService;
import za.co.standardbank.whatsapp.config.ApplicationConfigReader;

@Service
public class MessageListener {

    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);
    
    @Autowired
    ApplicationConfigReader applicationConfigReader;
    WhatsAppService whatsAppService = new WhatsAppService();

    @RabbitListener(queues = "${toWhatsapp.queue.name}")
    public void receiveMessageToWhatsapp(SendWhatsapp reqObj) 
    {
        log.info("Received message: {} from ToWhatsapp queue.", reqObj.getIntegrationId().toString());
        
       try 
        {
            log.info("Making REST call to the API");
            List<Messages> messages = reqObj.getMessages();
            for (Messages messages2 : messages) 
            {
               if(messages2.getTo().substring(1) == "0")
               {
                   String numString = messages2.getTo();
                   numString.replaceFirst("0", "27");
                   messages2.setTo(numString);
               }
            } 
            
            whatsAppService.SendMessage(reqObj);
            log.info("<< Exiting SendMessage after API call.");
        } 
        catch(HttpClientErrorException  ex) 
        {	
            if(ex.getStatusCode() == HttpStatus.NOT_FOUND) 
            {
                log.info("Delay...");
                
                try 
                {
                    Thread.sleep(ApplicationConstant.MESSAGE_RETRY_DELAY);
                } 
                catch (InterruptedException e) 
                { 
                    log.info(e.getMessage());
                }
                
                log.info("Throwing exception so that message will be requed in the queue.");
                // Note: Typically Application specific exception can be thrown below
                throw new RuntimeException();
            } 

            throw new AmqpRejectAndDontRequeueException(ex);     	
            
        } 
        catch(Exception e) 
        {
            log.error("Internal server error occurred in python server. Bypassing message requeue {}", e);
            throw new AmqpRejectAndDontRequeueException(e); 
        }

    }
}