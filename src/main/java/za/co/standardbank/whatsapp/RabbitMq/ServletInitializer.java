package za.co.standardbank.whatsapp.RabbitMq;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import za.co.standardbank.whatsapp.WhatsappserviceApplication;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WhatsappserviceApplication.class);
	}

}

